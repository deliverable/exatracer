# SPDX-FileCopyrightText: 2024 EfficiOS, Inc.
#
# SPDX-License-Identifier: MIT

include config.mk

# Do not touch below.
PKG_CONFIG_PATH := $(builddir)/lib/pkgconfig:$(PKG_CONFIG_PATH)

# HIP C compiler.
HIPCC=$(ROCM)/bin/hipcc

# This is used in some of the HIP headers.
PLATFORM=__HIP_PLATFORM_$(VENDOR)__

# Libside flags.
LIBSIDE_CFLAGS=$(shell PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config --cflags libside)
LIBSIDE_LIBS=$(shell PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config --libs libside)

# Rocprofiler-sdk flags.  There is not rocprofiler-sdk.pc for pkg-config.
ROCPROFILER_SDK_CFLAGS=-I $(ROCM)/include -L $(ROCM)/lib -Wl,-rpath=$(ROCM)/lib
ROCPROFILER_SDK_LIBS=-lrocprofiler-sdk

# Concat dependencies.
DEPS_CFLAGS=$(LIBSIDE_CFLAGS) $(ROCPROFILER_SDK_CFLAGS)
DEPS_LIBS=$(LIBSIDE_LIBS) $(ROCPROFILER_SDK_LIBS)

# HIP stuff.
AUTOGEN_HIP_API=$(builddir)/side-hip.h         \
                $(builddir)/side-hip-classes.h \
                $(builddir)/side-hip-states.h

HIP_HEADER=$(ROCM)/include/hip/hip_runtime.h

# HSA stuff.
AUTOGEN_HSA_API= $(builddir)/side-hsa.h         \
                $(builddir)/side-hsa-classes.h \
                $(builddir)/side-hsa-states.h

HSA_HEADER=$(ROCM)/include/hsa/hsa.h

# Final target.
TARGET=$(builddir)/libexatracer.so

# Toolchain flags.
CFLAGS=-I $(builddir) -D $(PLATFORM) -O2 -g -fmax-errors=1 -fvisibility=hidden -Wextra -Wno-deprecated-declarations -Wno-missing-field-initializers
LDFLAGS=-shared -fPIC

all: $(builddir) $(TARGET)

$(builddir):
	mkdir -p $@

# Do not add *-wrappers.cpp to toolchain inputs.
$(TARGET): src/side-exatracer.cpp
	$(CXX) -I src -std=gnu++17 $(CFLAGS) $(DEPS_CFLAGS) $(LDFLAGS) -Wl,-rpath=$(builddir)/lib -o $@ $^ $(DEPS_LIBS)

src/side-exatracer.cpp: $(builddir)/side-hsa-wrappers.cpp $(builddir)/side-hip-wrappers.cpp

# HIP rules.
$(builddir)/side-hip-wrappers.cpp: $(AUTOGEN_HIP_API) scripts/gen-hip-wrappers
	scripts/gen-hip-wrappers -D $(PLATFORM) -I $(ROCM)/include --ignore=src/hip-ignores.txt $(HIP_HEADER) $@
	sed -i -f scripts/hip-post-processing.sed $@

$(AUTOGEN_HIP_API) &: $(HIP_HEADER) scripts/side-auto-api
	scripts/side-auto-api --ignores=src/hip-ignores.txt -D $(PLATFORM) -I $(ROCM)/include --namespace=side_hip --provider=hip --common-prefix=hip --states-guard=SIDE_TRACEPOINT_HIP_STATES_H --classes-guard=SIDE_TRACEPOINT_HIP_CLASSES_H --tp-guard=SIDE_TRACEPOINT_HIP_DEFINITIONS_H $< $(AUTOGEN_HIP_API)
	sed -i -f scripts/hip-post-processing.sed $(AUTOGEN_HIP_API)

# HSA rules.
$(builddir)/side-hsa-wrappers.cpp: $(AUTOGEN_HSA_API) scripts/gen-hsa-wrappers
	scripts/gen-hsa-wrappers -D $(PLATFORM) -I $(ROCM)/include --ignore=src/hsa-ignores.txt $(HSA_HEADER) $@
	sed -i -f scripts/hsa-post-processing.sed $@

$(AUTOGEN_HSA_API) &: $(HSA_HEADER) scripts/side-auto-api
	scripts/side-auto-api --ignores=src/hsa-ignores.txt -D $(PLATFORM) -I $(ROCM)/include --namespace=side_hsa --provider=hsa --common-prefix=hsa --states-guard=SIDE_TRACEPOINT_HSA_STATES_H --classes-guard=SIDE_TRACEPOINT_HSA_CLASSES_H --tp-guard=SIDE_TRACEPOINT_HSA_DEFINITIONS_H $< $(AUTOGEN_HSA_API)
	sed -i -f scripts/hsa-post-processing.sed $(AUTOGEN_HSA_API)

# Testing.
$(builddir)/hello: tests/hello.cpp
	$(HIPCC) -lrocprofiler-sdk-roctx -Wno-unused -o $@ $^

check: $(builddir)/hello
	scripts/check $(TARGET) $^

clean:
	rm -rf $(builddir)
	rm -rf ./traces

dist:
	git archive --prefix exatracer/ --format=tar.gz --output exatracer.tar.gz HEAD

.PHONY: all clean dist
