<!--
SPDX-FileCopyrightText: 2024 EfficiOS, Inc.

SPDX-License-Identifier: MIT
-->

# Tutorial

This tutorial shows how to compile the Exatracer and use it for testing a HIP
hello world program.  Before starting the tutorial, make sure that the Exatracer
is built and that all required dependencies are met by following the
[Build](build.md) document.

# User program

The following is the program that will be instrumented.

hello-world.cpp:
```c
#include <stdlib.h>

#include <iostream>

#include <hip/hip_runtime.h>
#include <rocprofiler-sdk-roctx/roctx.h>

using namespace std;

__global__ void helloworld(char* in, char* out)
{
    int num = hipThreadIdx_x + hipBlockDim_x * hipBlockIdx_x;
    out[num] = in[num] + 1;
}

int main(int argc, char* argv[])
{

    hipDeviceProp_t devProp;

    roctx_range_id_t range = roctxRangeStart("hello");

    hipGetDeviceProperties(&devProp, 0);
    cout << " System minor " << devProp.minor << endl;
    cout << " System major " << devProp.major << endl;
    cout << " agent prop name " << devProp.name << endl;

    /* Initial input,output for the host and create memory objects for the kernel*/
    const char* input = "GdkknVnqkc";
    size_t strlength = strlen(input);
    cout << "input string:" << endl;
    cout << input << endl;
    char *output = (char*) malloc(strlength + 1);

    char* inputBuffer;
    char* outputBuffer;
    hipMalloc((void**)&inputBuffer, (strlength + 1) * sizeof(char));
    hipMalloc((void**)&outputBuffer, (strlength + 1) * sizeof(char));

    hipMemcpy(inputBuffer, input, (strlength + 1) * sizeof(char), hipMemcpyHostToDevice);

    hipLaunchKernelGGL(helloworld,
                       dim3(1),
                       dim3(strlength),
                       0, 0,
                       inputBuffer ,outputBuffer );

    hipMemcpy(output, outputBuffer,(strlength + 1) * sizeof(char), hipMemcpyDeviceToHost);

    hipFree(inputBuffer);
    hipFree(outputBuffer);

    output[strlength] = '\0';	//Add the terminal character to the end of output.
    cout << "\noutput string:" << endl;
    cout << output << endl;

    free(output);

    std::cout<<"Passed!\n";
    roctxRangeStop(range);
    return EXIT_SUCCESS;
}
```

The program is compiled with `hipcc`:
```sh
$ hipcc -lrocprofiler-sdk-roctx -o hello-world hello-world.cpp
```

# Compiling the Exatracer

The Exatracer can be configured with the variables in `config.mk`.  If ROCM is
installed under `/opt/rocm-*`, then it is enough to simply configure the
`ROCM_VERSION` variable.  For example, if the version of ROCM is 6.1.0 and
installed under `/opt/rocm-6.1.0`, then is is enought to configure
`ROCM_VERSION=6.1.0`.  You can also directly specify the ROCM directory with the
`ROCM` configuration variable.  For example `ROCM=/tmp/rocm`.

To compile the Exatracer, make sure that you have all dependencies installed as
listed in [Build](build.md).  Note that `python-clang` can be installed using
pip and the `requirements.txt` file, but it is better to use the corresponding
version install with the Clang toolchain when possible.

If Python clang failed to find libclang, you can provide the path to it using
the `SIDE_AUTO_API_CLANG_LIBRARY_FILE` environment variable.  For example:
```sh
make SIDE_AUTO_API_CLANG_LIBRARY_FILE=/usr/lib/x86_64-linux-gnu/libclang-18.so
```

Once all dependencies were installed and the project was configure, run `make`
to compile the Exatracer:
```sh
$ make
```
# Running the application

**NOTE:** If running the Exatracer in a container, make sure that enough shared
memory is available (e.g. docker --shm-size).  The amount of memory required is
proportional to the number of CPU on the machine.  If you don't see any events
being emitted while using the Exatracer, it might be because the container
shared memory was misconfigured or the LTTng session was misconfigured for the
amount of shared memory available in the container.

The following script shows how the Exatracer can be used to instrument the
`hello-world` program:

```sh
TRACE_OUTPUT=my-traces

# Create an LTTng session and dump the trace to `my-traces' directory.
lttng create --output $TRACE_OUTPUT

# Enable all events defined by the Exatracer.  For every public function of HIP,
# HSA there are two events.  `enter' and `exit'.  For every public function of
# ROC-TX, there is a single event.
#
# All events have some context information, like timestamp, hostname and CPU ID.
#
# An `enter' event has all the arguments passed to the function, plus a pair of
# IDs that when concatenated, forms a unique ID in the trace.
#
# An `exit' event only has the corresponding pair of IDs to do correlation with
# the `enter' event.
lttng enable-event --userspace 'hsa:*'
lttng enable-event --userspace 'hip:*'
lttng enable-event --userspace 'roctx:*'

# Starting the session is required to get the trace.
lttng start

# The application is executed with the Exatracer LD_PRELOAD onto it.  This
# assumes that `libexatracer.so' can be found by the dynamic loader.  See
# ld-linux(8) for more information.
LD_PRELOAD=libexatracer.so ./hello-world

# When done, the tracing session can be destroyed to flush all pending events.
lttng destroy

# The trace can now be observed with babeltrace or Trace Compass.
babeltrace2 $TRACE_OUTPUT
```

# The trace output

The output from Babeltrace should look like this:

```text
[12:35:27.220824605] (+?.?????????) HOST roctx:range_start: { cpu_id = 12 }, { message = "hello", id = 0x1 }
[12:35:27.220917503] (+0.000092898) HOST hip:hipGetDevicePropertiesR0600_entry: { cpu_id = 12 }, { lttng_thread_id = 0, lttng_local_id = 0, prop = 0x7FFF826EDC48, deviceId = 0 }
[12:35:27.230209340] (+0.009291837) HOST hip:hipGetDevicePropertiesR0600_exit: { cpu_id = 12 }, { lttng_thread_id = 0, lttng_local_id = 0, lttng_has_ret = 1, lttng_ret = 0 }
[12:35:27.230242559] (+0.000033219) HOST hip:hipMalloc_entry: { cpu_id = 12 }, { lttng_thread_id = 0, lttng_local_id = 1, ptr = 0x7FFF826EDBE8, size = 11 }
[12:35:27.230347527] (+0.000104968) HOST hip:hipMalloc_exit: { cpu_id = 12 }, { lttng_thread_id = 0, lttng_local_id = 1, lttng_has_ret = 1, lttng_ret = 0 }
[12:35:27.230347787] (+0.000000260) HOST hip:hipMalloc_entry: { cpu_id = 12 }, { lttng_thread_id = 0, lttng_local_id = 2, ptr = 0x7FFF826EDBE0, size = 11 }
[12:35:27.230350197] (+0.000002410) HOST hip:hipMalloc_exit: { cpu_id = 12 }, { lttng_thread_id = 0, lttng_local_id = 2, lttng_has_ret = 1, lttng_ret = 0 }
[12:35:27.230355997] (+0.000005800) HOST hip:hipMemcpy_entry: { cpu_id = 12 }, { lttng_thread_id = 0, lttng_local_id = 3, dst = 0x1101400000, src = 0x560F3711903A, sizeBytes = 11, kind = ( "hipMemcpyHostToDevice" : container = 1 ) }
[12:35:27.403624194] (+0.173268197) HOST hip:hipMemcpy_exit: { cpu_id = 11 }, { lttng_thread_id = 0, lttng_local_id = 3, lttng_has_ret = 1, lttng_ret = 0 }
[12:35:27.403636173] (+0.000011979) HOST hip:hipLaunchKernel_entry: { cpu_id = 11 }, { lttng_thread_id = 0, lttng_local_id = 4, function_address = 0x560F3711DD58, numBlocks_x = 1, numBlocks_y = 1, numBlocks_z = 1, dimBlocks_x = 10, dimBlocks_y = 1, dimBlocks_z = 1, args = 0x7FFF826EDC30, sharedMemBytes = 0, stream = 0x0 }
[12:35:27.403980817] (+0.000344644) HOST hip:hipLaunchKernel_exit: { cpu_id = 11 }, { lttng_thread_id = 0, lttng_local_id = 4, lttng_has_ret = 1, lttng_ret = 0 }
[12:35:27.403981597] (+0.000000780) HOST hip:hipMemcpy_entry: { cpu_id = 11 }, { lttng_thread_id = 0, lttng_local_id = 5, dst = 0x560F6720FB10, src = 0x1101401000, sizeBytes = 11, kind = ( "hipMemcpyDeviceToHost" : container = 2 ) }
[12:35:27.404287381] (+0.000305784) HOST hip:hipMemcpy_exit: { cpu_id = 11 }, { lttng_thread_id = 0, lttng_local_id = 5, lttng_has_ret = 1, lttng_ret = 0 }
[12:35:27.404289481] (+0.000002100) HOST hip:hipFree_entry: { cpu_id = 11 }, { lttng_thread_id = 0, lttng_local_id = 6, ptr = 0x1101400000 }
[12:35:27.404304100] (+0.000014619) HOST hip:hipFree_exit: { cpu_id = 11 }, { lttng_thread_id = 0, lttng_local_id = 6, lttng_has_ret = 1, lttng_ret = 0 }
[12:35:27.404304310] (+0.000000210) HOST hip:hipFree_entry: { cpu_id = 11 }, { lttng_thread_id = 0, lttng_local_id = 7, ptr = 0x1101401000 }
[12:35:27.404306480] (+0.000002170) HOST hip:hipFree_exit: { cpu_id = 11 }, { lttng_thread_id = 0, lttng_local_id = 7, lttng_has_ret = 1, lttng_ret = 0 }
[12:35:27.404321530] (+0.000015050) HOST roctx:range_stop: { cpu_id = 11 }, { id = 0x1 }
[12:35:27.404341170] (+0.000019640) HOST hip:hipDeviceSynchronize_entry: { cpu_id = 11 }, { lttng_thread_id = 0, lttng_local_id = 8 }
[12:35:27.404342780] (+0.000001610) HOST hip:hipDeviceSynchronize_exit: { cpu_id = 11 }, { lttng_thread_id = 0, lttng_local_id = 8, lttng_has_ret = 1, lttng_ret = 0 }
```

Each line represents an event.  Events are printed in chronological order.  The
anatomy of a event is like so:

`[TIMESTAMP] (PREVIOUS-DIFF) HOSTNAME PROVIDER:EVENT_NAME CONTEXT ... { FIELDS ... }`

Refer to the [Babeltrace
documentation](https://babeltrace.org/docs/v2.0/man1/babeltrace2-convert.1/) for
interpreting the events.
