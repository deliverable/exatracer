<!--
SPDX-FileCopyrightText: 2024 EfficiOS, Inc.

SPDX-License-Identifier: MIT
-->

# Prerequisite

## libexatracer.so

See [Build](build.md) to get the required shared library.

# Usage

The Exatracer is used by using the `LD_PRELOAD` environment variable.  Simply
add the absolute path to the shared library, or a relative path if its directory
is in `LD_LIBRARY_PATH`, before executing the application.

Example:

```sh
$ LD_PRELOAD=libexatracer.so ./my-application
```

# LTTng configuration

Loading the Exatracer is not enough to get a trace.  Events must be enabled in a
tracing session.  See the [LTTng documentation](https://lttng.org/docs/v2.13/)
on how to use LTTng, or see the provided [Tutorial](tutorial.md).
