<!--
SPDX-FileCopyrightText: 2024 EfficiOS, Inc.

SPDX-License-Identifier: MIT
-->

# Building

## Prerequisites

- **[hipamd](https://github.com/Rocm/clr) >= 6.2.0**
- **[lttng-ust](https://lttng.org/) >= 2.13.7**
- **[make](https://www.gnu.org/software/make/) >= 4.3**
- **[pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/) >= 0.29.2**
- **[python-clang](https://clang.llvm.org) >= 13.0.1**
- **[rocm-toolchain](https://github.com/ROCm/llvm-project) >= 6.2.0**
- **[rocprofiler-sdk](https://github.com/ROCm/rocprofiler-sdk) >= 6.2.0**
- **[rocr-runtime](https://github.com/ROCm/rocr-runtime) >= 6.2.0**

## Optional dependencies

- **[babeltrace](https://babeltrace.org/) >= 2.0.5**
- **[lttng-tools](https://lttng.org/) >= 2.13.11**

## Build steps

### Configuration

Edit the variables under `config.mk` to configure the project.

#### Guix profiles

If using Guix, run the script `scripts/config-guix` to setup the correct
environment variables for the toolchain.

### Compilation

Call `make`.  The shared library `libexatracer.so` should now exist under the
build directory.

If Python clang failed to find libclang, you can provide the path to it using
the `SIDE_AUTO_API_CLANG_LIBRARY_FILE` environment variable.  For example:
```sh
make SIDE_AUTO_API_CLANG_LIBRARY_FILE=/usr/lib/x86_64-linux-gnu/libclang-18.so
```

### Testing

Call `make check` to run the test.  A test program compiled with `hipcc` is
executed with the Exatracer.

### Installing

Not yet supported.
