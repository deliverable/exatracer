# Version of ROCM to use.
ROCM_VERSION?=6.2.0

# Path to ROCM directory.
ROCM?=/opt/rocm-$(ROCM_VERSION)

# Hardware vendor for ROCM.
VENDOR?=AMD

# C++ toolchain to compile the Exatracer.
CXX?=clang++

# Build directory where all outputs will be generated.
builddir?=$(CURDIR)/build
