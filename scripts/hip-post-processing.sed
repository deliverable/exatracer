# SPDX-FileCopyrightText: 2023 EfficiOS, Inc.
#
# SPDX-License-Identifier: MIT
s/hipChooseDeviceR[0-9]*/hipChooseDevice/g
