;;; Copyright © 2023 Advanced Micro Devices, Inc.
;;; Copyright © 2024 David Elsing <david.elsing@posteo.net>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-rocm packages rocm-base)
  #:use-module (srfi srfi-1)

  #:use-module (guix gexp)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system trivial)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)

  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages python)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages vim))


(define-public %rocm-version "6.2.0")


;; GPU targets to build device code for, separated by semicolons. This
;; is used by all ROCm packages which include device code.
(define-public (%amdgpu-targets)
  "gfx1101")

;; The same as %amdgpu-targets, separated by spaces.
(define-public (%amdgpu-targets-sp)
  (string-join (string-split (%amdgpu-targets) #\;) " "))
;; The same as %amdgpu-targets, without gfx prefix.
(define-public (%amdgpu-targets-nogfx)
  (string-join (map
				(lambda (s) (substring s 3))
				(string-split (%amdgpu-targets) #\;)) " "))
;; The same as %amdgpu-targets-nogfx, separated by spaces.
(define-public (%amdgpu-targets-nogfx-sp)
  (string-join (string-split (%amdgpu-targets-nogfx) #\;) " "))


(define-public libffi-shared
  (package
    (inherit libffi)
    (arguments
     (list
      #:phases #~(modify-phases %standard-phases
                   (add-after 'unpack 'set-CFLAGS
                     (lambda _
                       (setenv "CFLAGS" " -fPIC"))))))))

(define-public llvm-rocm
  (package
    (inherit llvm-18)
    (name "llvm-rocm")
    (version %rocm-version)
    (source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url (string-append "https://github.com/ROCm/llvm-project"))
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32 "03smwbqrrkmdc22j4bi7c6vq5mxmmlv96xnqnmd4fbm89r59sh6c"))
	   (patches
		(search-patches "guix-rocm/packages/patches/llvm-rocm.patch"))))
    (inputs (modify-inputs (package-inputs llvm-18)
              (replace "libffi" libffi-shared)))))

(define-public clang-runtime-rocm
  (package
	(inherit clang-runtime-18)
	(name "clang-runtime-rocm")
	(source (package-source llvm-rocm))
	(inputs (modify-inputs (package-inputs clang-runtime-18)
			  (replace "llvm" llvm-rocm)
			  (replace "libffi" libffi-shared)))))

(define-public clang-rocm
  (package
	(inherit clang-18)
	(name "clang-rocm")
	(version (package-version llvm-rocm))
	(source (package-source llvm-rocm))
	(inputs (modify-inputs (package-inputs clang-18)
			  (delete "clang-tools-extra")))
	(propagated-inputs (modify-inputs (package-propagated-inputs clang-18)
						 (replace "llvm" llvm-rocm)
						 (replace "clang-runtime" clang-runtime-rocm)))
	(arguments
	 (substitute-keyword-arguments (package-arguments clang-18)
	   ((#:phases phases '(@ () %standard-phases))
		#~(modify-phases #$phases
			(replace 'add-tools-extra
			  (lambda _
				(copy-recursively "../clang-tools-extra" "tools/extra")))))))))

(define-public lld-rocm
  (package
	(inherit lld-18)
	(name "lld-rocm")
	(version (package-version llvm-rocm))
	(source (package-source llvm-rocm))
	(inputs (list llvm-rocm))))

(define-public rocm-device-libs
  (package
	(name "rocm-device-libs")
	(version (package-version llvm-rocm))
	(source (package-source llvm-rocm))
	(build-system cmake-build-system)
	(arguments
	 (list
	  #:build-type "Release"
	  #:tests? #f
	  #:phases
	  #~(modify-phases %standard-phases
		  (add-after 'unpack 'ockl_ocml_irif_inc
			(lambda _
			  (copy-recursively "irif/inc"
								(string-append #$output "/irif/inc"))
			  (copy-recursively "oclc/inc"
								(string-append #$output "/oclc/inc"))
			  (copy-recursively "ockl/inc"
								(string-append #$output "/ockl/inc"))))
		  (add-after 'unpack 'chdir
			(lambda _
			  (chdir "amd/device-libs"))))))
	(native-inputs (list clang-rocm))
	(synopsis "ROCm Device libraries")
	(description
	 "This repository contains the sources and CMake build system for
a set of AMD specific device-side language runtime libraries.")
	(home-page "https://github.com/RadeonOpenCompute/ROCm-Device-Libs.git")
	(license license:ncsa)))

(define-public roct-thunk-interface
  (package
	(name "roct-thunk-interface")
	(version %rocm-version)
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url (string-append "https://github.com/ROCm/roct-thunk-interface"))
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32 "0zahlp61lkwcs27lgb0rbvxaxgpfkaz1f715j075whmzvsfp9kj1"))))
	(build-system cmake-build-system)
	(arguments
	 (list
	  #:build-type "Release"
	  #:tests? #f))
	(inputs (list libdrm numactl))
	(native-inputs (list `(,gcc "lib") pkg-config))
	(synopsis "ROCT Thunk interface")
	(description "This repository includes the user-mode API interfaces used
to interact with the ROCk driver.")
	(home-page "https://github.com/RadeonOpenCompute/ROCT-Thunk-Interface.git")
	(license license:expat)))

(define-public rocr-runtime
  (package
	(name "rocr-runtime")
	(version %rocm-version)
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url (string-append "https://github.com/ROCm/rocr-runtime"))
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32 "0164gdlygyhvpik3bdf9ykrb3q70vvwgjlnvvh7wfr4k4lin4b0m"))
	   (patches
		(search-patches "guix-rocm/packages/patches/rocr-runtime-rocm-device-libs.patch"))))
	(build-system cmake-build-system)
	(arguments
	 (list
      #:build-type "Release"
      #:tests? #f
      #:configure-flags
	  #~(list (string-append
			   "-DBITCODE_DIR="
			   #$(this-package-input "rocm-device-libs") "/amdgcn/bitcode/"))
	  #:phases #~(modify-phases %standard-phases
                   (add-after 'unpack 'chdir
					 (lambda _
                       (chdir "src"))))))
	(inputs (list numactl roct-thunk-interface rocm-device-libs libdrm libffi))
	(native-inputs (list xxd libelf lld-rocm clang-rocm pkg-config))
	(synopsis "HSA Runtime API and runtime for ROCm")
	(description
	 "This repository includes the user-mode API interfaces and
libraries necessary for host applications to launch compute kernels to
available HSA ROCm kernel agents. Reference source code for the core
runtime is also available.")
	(home-page "https://github.com/RadeonOpenCompute/ROCR-Runtime.git")
	(license license:ncsa)))

(define-public lld-wrapper-rocm
  (make-lld-wrapper lld-rocm))

(define-public libomp-rocm
  (package
    (inherit libomp-18)
    (name "libomp-rocm")
    (version (package-version llvm-rocm))
    (source (package-source llvm-rocm))
	(native-inputs
	 (modify-inputs (package-native-inputs libomp-18)
	   (prepend gcc)
	   (replace "clang" clang-rocm)
	   (replace "llvm" llvm-rocm)
	   (replace "python" python-wrapper)
	   (append lld-wrapper-rocm)
	   (append elfutils)))
	(inputs
	 (modify-inputs (package-inputs libomp-18)
       (append libdrm)
       (append numactl)
       (append roct-thunk-interface)
       (append libffi-shared)
       (append rocm-device-libs)
       (append rocr-runtime)))
    (arguments
     (substitute-keyword-arguments (package-arguments libomp-18)
       ((#:configure-flags flags)
        #~(append
		   (list "-DOPENMP_ENABLE_LIBOMPTARGET=1"
				 "-DCMAKE_C_COMPILER=clang"
				 "-DCMAKE_CXX_COMPILER=clang++"
				 "-DCMAKE_BUILD_TYPE=Release"
				 "-DCMAKE_EXE_LINKER_FLAGS=-fuse-ld=lld" ; can be removed if we use lld-as-ld-wrapper
				 "-DCMAKE_SHARED_LINKER_FLAGS=-fuse-ld=lld" ; can be removed if we use lld-as-ld-wrapper
				 (string-append
				  "-DLIBOMPTARGET_AMDGCN_GFXLIST=" #$(%amdgpu-targets))
				 (string-append "-DDEVICELIBS_ROOT="
								#$(this-package-input "rocm-device-libs"))
				 (string-append "-DLLVM_DIR="
								#$(this-package-native-input "llvm")))
		   #$flags))
	   ((#:phases phases '%standard-phases)
        #~(modify-phases #$phases
            (add-after 'set-paths 'adjust-LD_LIBRARY_PATH
              (lambda _
                (setenv
				 "LD_LIBRARY_PATH"
				 (string-append
				  #$(this-package-native-input "llvm") "/lib" ":"
				  (ungexp (this-package-native-input "gcc") "lib") "/lib"))))
            (add-after 'unpack 'patch-clang-tools
              (lambda _
                (substitute*
		 (list "openmp/libomptarget/CMakeLists.txt"
		       "openmp/libomptarget/DeviceRTL/CMakeLists.txt")
                  (("find_program\\(CLANG_TOOL clang PATHS \\$\\{LLVM_TOOLS_BINARY_DIR\\}")
                   (string-append "find_program(CLANG_TOOL clang PATHS "
                                  #$(this-package-native-input "clang") "/bin"))
                  (("find_program\\(CLANG_OFFLOAD_BUNDLER_TOOL clang-offload-bundler PATHS \\$\\{LLVM_TOOLS_BINARY_DIR\\}")
                   (string-append
                    "find_program(CLANG_OFFLOAD_BUNDLER_TOOL clang-offload-bundler PATHS "
                    #$(this-package-native-input "clang") "/bin"))
                  (("find_program\\(PACKAGER_TOOL clang-offload-packager PATHS \\$\\{LLVM_TOOLS_BINARY_DIR\\}")
                   (string-append
                    "find_program(PACKAGER_TOOL clang-offload-packager PATHS "
                    #$(this-package-native-input "clang") "/bin")))))))))))

(define-public rocm-toolchain
  (let ((rocm-clang-toolchain (make-clang-toolchain clang-rocm libomp-rocm)))
	(package
	  (inherit rocm-clang-toolchain)
	  (name "rocm-toolchain")
	  (version (package-version llvm-rocm))
	  (inputs
	   (modify-inputs (package-inputs rocm-clang-toolchain)
		 (append lld-wrapper-rocm
				 rocr-runtime
				 rocm-device-libs
				 roct-thunk-interface)))
	  (synopsis
	   "Complete ROCm toolchain, based on the Clang toolchain, for
C/C++ development")
	  (description
	   "This package provides a complete ROCm toolchain for C/C++
development to be installed in user profiles. This includes Clang, as
well as libc (headers and binaries, plus debugging symbols in the
@code{debug} output), Binutils, the ROCm device libraries, and the
ROCr runtime."))))

(define-public hipify
  (package
	(name "hipify")
	(version (package-version clang-rocm))
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url (string-append "https://github.com/ROCm/hipify"))
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name (string-append "rocm-" version)))
	   (sha256 "1isdc5qv21f0x052m1n7f2xfqi3vbp88f5622hh2rklmfb40cjxh")))
	(build-system cmake-build-system)
	(arguments
	 (list
      #:build-type "Release"
      #:tests? #f
      #:configure-flags
	  ''("-DCMAKE_CXX_COMPILER=clang++")
	  #:phases
	  '(modify-phases %standard-phases
		 (add-before 'configure 'prepare-cmake
		   (lambda _
			 (substitute* "CMakeLists.txt"
			   (("set.CMAKE_CXX_COMPILER.*") "")
			   (("set.CMAKE_C_COMPILER.*") "")))))))
	(inputs (list clang-rocm perl))
	(synopsis "HIPIFY: Convert CUDA to HIP code.")
	(description
	 "HIPIFY is a set of tools that you can use to automatically
translate CUDA source code into portable HIP C++.")
	(home-page "https://github.com/ROCm/HIPIFY")
	(license license:ncsa)))

(define-public rocm-core
  (package
	(name "rocm-core")
	(version %rocm-version)
	(source (origin
			  (method git-fetch)
			  (uri (git-reference
					(url "https://github.com/Rocm/rocm-core")
					(commit (string-append "rocm-" version))))
			  (file-name (git-file-name name version))
			  (sha256
			   (base32
				"05wvl55zpp0awg6hkynsvywyqkgdw9bwzv0kbwnv1ii0a8z2xl1i"))))
	(build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:configure-flags
	  ''("-DROCM_VERSION=6.2.0")))
	(home-page "https://github.com/Rocm/rocm-core")
	(synopsis "Utility to get the ROCm release version")
	(description "Utility to get the ROCm release version")
	(license license:expat)))
