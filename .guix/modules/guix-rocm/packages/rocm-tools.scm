;;; Copyright © 2023 Advanced Micro Devices, Inc.
;;; Copyright © 2024 David Elsing <david.elsing@posteo.net>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-rocm packages rocm-tools)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system python)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix transformations)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages gdb)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages serialization)

  #:use-module (guix-rocm packages rocm-base)
  #:use-module (guix-rocm packages rocm-hip))

(define-public rocminfo
  (package
    (name "rocminfo")
    (version %rocm-version)
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url (string-append "https://github.com/ROCm/rocminfo"))
	     (commit (string-append "rocm-" version))))
       (file-name (git-file-name name version))
       (sha256
	(base32
	 "0x0l87741bar4gscj3p0kdjbp7f88kvqh2w96lliwwzdv95fmgsa"))))
    (build-system cmake-build-system)
    (arguments
     `(#:build-type "Release"
       #:tests? #f))
    (inputs (list rocr-runtime python))
    (propagated-inputs (list grep kmod))
    (synopsis "ROCm application for reporting system info")
    (description "List @acronym{HSA,Heterogeneous System Architecture}
Agents available to ROCm and show their properties.")
    (home-page "https://github.com/RadeonOpenCompute/rocminfo")
    (license license:ncsa)))

(define-public rocm-bandwidth-test
  (package
    (name "rocm-bandwidth-test")
    (version %rocm-version)
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url (string-append "https://github.com/ROCm/rocm_bandwidth_test"))
	     (commit (string-append "rocm-" version))))
       (file-name (git-file-name name version))
       (sha256
	(base32
	 "0b5jrf87wa5dqmipdc4wmr63g31hhgn5ikcl6qgbb51w2gq0vvya"))
       (patches
	(search-patches
	 "guix-rocm/packages/patches/rocm-bandwidth-test-fix-includes.patch"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:tests? #f
      #:configure-flags
      ''("-DCMAKE_CXX_FLAGS=-Wno-error=cpp")))
    (inputs (list rocr-runtime))
    (synopsis "Bandwith test for ROCm")
    (description "RocBandwidthTest is designed to capture the
performance characteristics of buffer copying and kernel read/write
operations.  The help screen of the benchmark shows various options one
can use in initiating copy/read/writer operations.  In addition one can
also query the topology of the system in terms of memory pools and
their agents.")
    (home-page "https://github.com/ROCm/rocm_bandwidth_test")
    (license license:ncsa)))

(define-public rocm-smi
  (package
    (name "rocm-smi")
    (version %rocm-version)
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url (string-append "https://github.com/ROCm/rocm_smi_lib"))
	     (commit (string-append "rocm-" version))))
       (file-name (git-file-name name version))
       (sha256
	(base32
	 "0f73k2da53hwylwf9basmd8wla8wjcdsvrggh2ccv4z9lpy319wf"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:tests? #f
      #:build-type "Release"))
    (inputs (list python))
    (propagated-inputs (list grep coreutils))
    (synopsis "The ROCm System Management Interface (ROCm SMI) Library")
    (description "The ROCm System Management Interface Library, or
ROCm SMI library, is part of the Radeon Open Compute ROCm software
stack.  It is a C library for Linux that provides a user space
interface for applications to monitor and control GPU applications.")
    (home-page "https://github.com/RadeonOpenCompute/rocm_smi_lib.git")
    (license license:ncsa)))

(define-public tensile
  (package
    (name "tensile")
    (version %rocm-version)
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url (string-append "https://github.com/ROCm/tensile"))
	     (commit (string-append "rocm-" version))))
       (file-name (git-file-name name version))
       (sha256
	(base32
	 "1b5m6cjgmvcmahkj6mgzzxxg47fmnn74j9jj6dr1kfgxlaj78qmz"))
       (patches
	(search-patches
	 "guix-rocm/packages/patches/tensile-copy-if-not-exist.patch"))))
    (build-system python-build-system)
    (native-inputs (list python-pandas))
    (propagated-inputs
     (list msgpack-3
	   python-msgpack
	   python-pyyaml
	   python-joblib
	   python-psutil))
    (synopsis "A GEMM kernel generator for AMD GPUs.")
    (description "Tensile is a tool for creating benchmark-driven
backend libraries for GEMMs, GEMM-like problems (such as batched
GEMM), and general N-dimensional tensor contractions on a GPU.  The
Tensile library is mainly used as backend library to rocBLAS.  Tensile
acts as the performance backbone for a wide variety of 'compute'
applications running on AMD GPUs.")
    (home-page "https://github.com/ROCmSoftwarePlatform/Tensile.git")
    (license license:expat)))

(define-public rocdbgapi
  (package
    (name "rocdbgapi")
    (version %rocm-version)
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url (string-append "https://github.com/ROCm/rocdbgapi"))
	     (commit (string-append "rocm-" version))))
       (file-name (git-file-name name version))
       (sha256
	(base32
	 "1n6hdkv9agws5yzj79bff1j32b43caz6h5v1nb4cjjxqxb4hl04k"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:tests? #f
      #:build-type "Release"
      #:phases
      #~(modify-phases %standard-phases
	  (add-after 'unpack 'set-pci.ids-path
	    (lambda _
	      (substitute* "CMakeLists.txt"
		(("/usr/share/hwdata")
		 (string-append
		  (ungexp (this-package-native-input "hwdata") "pci")
		  "/share/hwdata"))))))))
    (inputs
     (list rocm-comgr
	   rocr-runtime))
    (native-inputs
     (list `(,hwdata "pci")
	   rocm-cmake))
    (synopsis "AMD Debugger API")
    (description "The AMD Debugger API is a library that provides all
the support necessary for a debugger and other tools to perform low
level control of the execution and inspection of execution state of
AMD's commercially available GPU architectures.")
    (home-page "https://github.com/ROCm/ROCdbgapi")
    (license license:expat)))

(define-public glog-0.7.1
  ((options->transformation
    `((with-version . "glog=0.7.1")
      (without-tests . "glog")))
   (@ (gnu packages logging) glog)))

(define-public ptl
  (package
    (name "PTL")
    (version "v2.3.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url (string-append "https://github.com/jrmadsen/PTL"))
	     (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1h78cvfdb01jv7kz4anns1gw6hh8if4f9n3mllbhp3gl3yhj314y"))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f
       #:build-type "Release"
       #:configure-flags '("-DPTL_USE_TBB=OFF"
                           "-DPTL_USE_GPU=OFF"
                           "-DPTL_USE_LOCKS=ON"
                           "-DPTL_BUILD_EXAMPLES=OFF")))
    (synopsis "")
    (description "")
    (home-page "")
    (license license:expat)))

(define-public rocprofiler-sdk
  (package
    (name "rocprofiler-sdk")
    (version %rocm-version)
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url (string-append "https://github.com/oldiob/rocprofiler-sdk.git"))
             (commit (string-append "rocm-" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1rryzp1d1cc63f6kgr7p38yn7w97giyk2l4kznc723kyhwkkzqk4"))))
    (build-system cmake-build-system)
    (inputs
     (list hipamd
           ptl
           rocr-runtime
           (@ (gnu packages serialization) cereal)
           (@ (gnu packages xdisorg) libdrm)
           (@ (gnu packages elf) elfutils)
           (@ (gnu packages pretty-print) fmt)
           glog-0.7.1))
    (arguments
     (list
      #:tests? #f
      #:build-type "Release"
      #:configure-flags ''("-DROCPROFILER_BUILD_GLOG=OFF"
                           "-DROCPROFILER_BUILD_FMT=OFF"
                           "-DROCPROFILER_BUILD_GHC_FS=OFF")))
    (synopsis "")
    (description "")
    (home-page "")
    (license license:expat)))

(define-public rocprofiler-register
  (package
    (name "rocprofiler-register")
    (version %rocm-version)
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url (string-append "https://github.com/ROCm/rocprofiler-register"))
	     (commit (string-append "rocm-" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "02cxwx3v71x2sdaffhfy5vnyq5fg9lr6n4995fryy2w4szw7qvzq"))))
    (build-system cmake-build-system)
    (inputs
     (list (@ (gnu packages pretty-print) fmt)
           (@ (gnu packages logging) glog)))
    (arguments
     (list
      #:tests? #f
      #:build-type "Release"
      #:phases
      '(modify-phases %standard-phases
         (add-after 'unpack 'no-external
           (lambda _
             (substitute* "CMakeLists.txt"
               (("include\\(rocprofiler_register_config_packaging\\)") ""))
             (substitute* "CMakeLists.txt"
               (("add_subdirectory\\(external\\)")
                "find_package(glog REQUIRED)\nfind_package(fmt REQUIRED)")))))))
    (synopsis "")
    (description "")
    (home-page "")
    (license license:expat)))

(define-public rocgdb
  (package
    (inherit gdb)
    (name "rocgdb")
    (version %rocm-version)
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url (string-append "https://github.com/ROCm/rocgdb"))
	     (commit (string-append "rocm-" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1chrxshq0355xaz60wcl5mqnwvffn57yl08cmb7cxns1jl2vixz3"))))
    (inputs
     (modify-inputs (package-inputs gdb)
       (prepend rocdbgapi)))
    (native-inputs
     (modify-inputs (package-native-inputs gdb)
       (prepend bison flex perl)))
    (arguments
     (substitute-keyword-arguments (package-arguments gdb)
       ((#:configure-flags _ '())
	''("--program-prefix=roc"
	   "--disable-binutils"
	   "--disable-gprofng"
	   "--disable-gprof"
	   "--enable-tui"
	   "--enable-64-bit-bfd"
	   "--enable-targets=x86_64-linux-gnu,amdgcn-amd-amdhsa"
	   "--with-system-readline"
	   "--with-expat"
	   "--with-system-zlib"
	   "--with-lzma"
	   "--disable-gdbtk"
	   "--disable-ld"
	   "--disable-gas"
	   "--disable-gdbserver"
	   "--disable-sim"))))
    (synopsis "ROCm source-level debugger for Linux based on GDB")
    (description "The AMD ROCm Debugger (ROCgdb) is the AMD
source-level debugger for Linux, based on the GNU Debugger (GDB).")
    (home-page "https://github.com/ROCm/ROCgdb")
    (license license:gpl3+)))
