;;; Copyright © 2024 Advanced Micro Devices, Inc.
;;; Copyright © 2024 David Elsing <david.elsing@posteo.net>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-rocm packages rocm-libs)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)

  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages fabric-management)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages instrumentation)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)

  #:use-module (guix-rocm packages rocm-base)
  #:use-module (guix-rocm packages rocm-hip)
  #:use-module (guix-rocm packages rocm-tools))

(define-public rocrand
  (package
    (name "rocrand")
    (version %rocm-version)
    (source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/ROCm/rocRAND")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "0q2fc8pr3my4v58dxgdi63a39dclgi4403vzp3z0hpjs6l8pnm0f"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets)))
	  #:tests? #f))
	(inputs (list hipamd))
	(native-inputs (list rocm-cmake))
    (home-page "https://github.com/ROCm/rocRAND")
    (synopsis "RAND library for the HIP programming language")
    (description "This package contains implementations for
pseudorandom and quasirandom number generation in HIP.")
    (license license:expat)))

(define-public hiprand
  (package
    (name "hiprand")
    (version %rocm-version)
    (source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/ROCm/hipRAND")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "172947v56za1hrlwa84xz0sq9wdcmmj97dhl072wp1ckqb340j2f"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets)))))
	(inputs (list hipamd rocrand))
	(native-inputs (list rocm-cmake))
    (home-page "https://github.com/ROCm/hipRAND")
    (synopsis "RAND marshalling library with multiple supported backends")
    (description "This package contains a wrapper around rocRAND.")
    (license license:expat)))

(define-public rocblas
  (package
    (name "rocblas")
    (version %rocm-version)
    (source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/rocm/rocBLAS")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "1q1igpd837k94vx6qp6g5cpigpxc88f1x93g2v8h156368gscpsh"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:build-type "Release"
	  #:validate-runpath? #f
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets))
		 "-DBUILD_WITH_PIP=OFF")))
	(inputs
	 (list hipamd
		   msgpack-cxx
		   python
		   tensile))
	(native-inputs (list rocm-cmake))
    (home-page "https://github.com/rocm/rocBLAS")
    (synopsis "BLAS implementation for ROCm")
    (description "rocBLAS is the ROCm Basic Linear Algebra Subprograms
(BLAS) library. It is implemented in the HIP programming language.")
    (license (list license:expat license:bsd-3))))

(define-public rocprim
  (package
    (name "rocprim")
    (version %rocm-version)
	(source (origin
			  (method git-fetch)
			  (uri (git-reference
					(url "https://github.com/rocm/rocPRIM")
					(commit (string-append "rocm-" version))))
			  (file-name (git-file-name name version))
			  (sha256
			   (base32
				"0sj4r3jh1gvrjp5hjmbdpnd5545fl1qc7ginlpf1f52g18zq2rxq"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:build-type "Release"
      #:tests? #f ;No tests.
      #:configure-flags
	  #~(list (string-append "-DCMAKE_CXX_COMPILER="
                             #$(this-package-input "hipamd")
                             "/bin/hipcc")
			  #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets)))))
    (inputs (list hipamd))
    (native-inputs (list rocm-cmake))
    (synopsis
     "rocPRIM: a header-only library providing HIP parallel primitives")
    (description
     "The rocPRIM is a header-only library providing HIP parallel primitives 
for developing performant GPU-accelerated code on the AMD ROCm platform.")
    (home-page "https://github.com/ROCmSoftwarePlatform/rocPRIM.git")
    (license license:expat)))

(define-public rocsparse
  (package
    (name "rocsparse")
    (version %rocm-version)
    (source (origin
			  (method git-fetch)
			  (uri (git-reference
					(url "https://github.com/rocm/rocSPARSE")
					(commit (string-append "rocm-" version))))
			  (file-name (git-file-name name version))
			  (sha256
			   (base32
				"0j8ncvfr1gqgxdsbcsxqg1br8m2v4whb8kkw5qh4mqs3szppy4ys"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 (string-append "-DCMAKE_Fortran_COMPILER="
						#$(this-package-native-input "gfortran")
						"/bin/gfortran")
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets)))))
	(inputs
	 (list hipamd rocprim))
	(native-inputs (list gfortran rocm-cmake))
    (home-page "https://github.com/rocm/rocBLAS")
    (synopsis "BLAS implementation for ROCm")
    (description "rocBLAS is the ROCm Basic Linear Algebra Subprograms
(BLAS) library. It is implemented in the HIP programming language.")
    (license (list license:expat license:bsd-3))))

(define-public hipsparse
  (package
    (name "hipsparse")
    (version %rocm-version)
    (source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/ROCm/hipSPARSE")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "1zx2656mwb2r0dxfvr5q7ya61skh8miky5n42v40jncmzmjn7a3f"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets)))
	  #:phases
	  #~(modify-phases %standard-phases
		  (add-after 'unpack 'fix-experimental
			(lambda _
			  (substitute* "clients/common/utility.cpp"
				(("#ifdef __cpp_lib_filesystem")
				 "#if 1"))))
		  (add-after 'unpack 'set-cxx-standard
			(lambda _
			  (substitute* "CMakeLists.txt"
				(("set\\(CMAKE_CXX_STANDARD 14")
				 "set(CMAKE_CXX_STANDARD 17"))))
		  (add-after 'unpack 'set-example-fpie
			(lambda _
			  (substitute* "clients/samples/CMakeLists.txt"
				(("add_executable.*EXAMPLE_TARGET.*" all)
				 (string-append
				  all "target_compile_options(${EXAMPLE_TARGET}"
				  " PRIVATE -fpie)\n"))))))))
	(inputs (list hipamd rocsparse))
	(native-inputs (list gfortran rocm-cmake))
    (home-page "https://github.com/ROCm/hipSPARSE")
    (synopsis "SPARSE marshalling library with multiple supported backends")
    (description "This package contains a wrapper around rocSPARSE.")
    (license license:expat)))

(define-public rocsolver
  (package
    (name "rocsolver")
    (version %rocm-version)
    (source (origin
			  (method git-fetch)
			  (uri (git-reference
					(url "https://github.com/ROCm/rocSOLVER")
					(commit (string-append "rocm-" version))))
			  (file-name (git-file-name name version))
			  (sha256
			   (base32
				"1h23k9r6ghdb6l0v7yscyfss076jq0gm17wz24nqylxp3h5g85z6"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets)))
	  #:phases
	  #~(modify-phases %standard-phases
		  (add-after 'unpack 'fix-rocblas-complex
			(lambda _
			  (substitute* "library/src/lapack/roclapack_syevj_heevj.hpp"
				(("std::norm\\((.*)\\)" _ arg)
				 (string-append "std::norm(rocblas_complex_num(" arg "))"))))))))
	(inputs (list hipamd rocblas rocsparse))
	(native-inputs (list fmt rocm-cmake))
    (home-page "https://github.com/ROCm/rocSOLVER")
    (synopsis "LAPACK implementation for ROCm")
    (description "LAPACK implementation for ROCm")
    (license license:bsd-2)))

(define-public hipsolver
  (package
	(name "hipsolver")
	(version %rocm-version)
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/ROCm/hipSOLVER")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "084fajg3npxsj6nb1ll1mbdrjq8jkgz4ggdp6lkp8fjjlfq289ip"))))
	(build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets)))
	  #:phases
	  #~(modify-phases %standard-phases
		  (add-after 'unpack 'suitesparse-include
			(lambda _
			  (substitute* "library/src/amd_detail/hipsolver_sparse.cpp"
				(("<suitesparse/cholmod\\.h>")
				 "<cholmod.h>")))))))
	(inputs (list
			 hipamd
			 rocblas
			 rocsolver
			 rocsparse
			 suitesparse-config
			 suitesparse-cholmod))
	(native-inputs (list gfortran pkg-config rocm-cmake))
	(home-page "https://github.com/ROCm/hipSOLVER")
	(synopsis "ROCm SOLVER marshalling library")
	(description "ROCm SOLVER marshalling library")
	(license license:expat)))

(define-public hipblas
  (package
    (name "hipblas")
    (version %rocm-version)
    (source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/ROCm/hipBLAS")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "1nkw3fzr9sfppsc4wkr5mlgdh442b8hi0pnsw6p2py8ircdfk9j9"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
      #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets)))))
	(inputs
	 (list
	  hipamd
	  rocrand
	  rocsparse
	  rocblas
	  rocsolver))
	(native-inputs (list rocm-cmake gfortran))
    (home-page "https://github.com/ROCm/hipBLAS")
    (synopsis "ROCm BLAS marshalling library ")
    (description "ROCm BLAS marshalling library ")
    (license license:expat)))

(define-public rccl
  (package
    (name "rccl")
    (version %rocm-version)
    (source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/ROCm/rccl")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "1cr7fngr9g3rghrn840kdfqcnw1c822gd79igbx78l9vf4x60yql"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
      #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc"))
	  #:phases
	  #~(modify-phases %standard-phases
		  (add-after 'unpack 'patch-rocm-version
			(lambda _
			  (substitute* "CMakeLists.txt"
				(("cat \\$\\{ROCM_PATH\\}/\\.info/version")
				 (string-append "echo " #$%rocm-version))
				(("set\\(AMDGPU_TARGETS.*")
				 #$(string-append
					"set(AMDGPU_TARGETS " (%amdgpu-targets) ")\n"))))))))
	(inputs
	 (list
	  hipamd
	  hipify
	  rocm-smi))
	(native-inputs (list rocm-cmake))
    (home-page "https://github.com/ROCm/rccl")
    (synopsis "ROCm Communication Collectives Library")
    (description "ROCm Communication Collectives Library")
    (license license:bsd-3)))

(define-public hipcub
  (package
    (name "hipcub")
    (version %rocm-version)
    (source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/ROCm/hipCUB")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "0xa79218gkikf82x7iz0bdfdhm782pm2mqfv99lh5c5ak6jf76bw"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
      #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets)))))
	(inputs (list hipamd rocprim))
	(native-inputs (list rocm-cmake))
    (home-page "https://github.com/ROCm/hipCUB")
    (synopsis "Reusable software components for ROCm developers")
    (description "Reusable software components for ROCm developers")
    (license license:bsd-3)))

(define-public rocthrust
  (package
    (name "rocthrust")
    (version %rocm-version)
    (source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/ROCm/rocThrust")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "09irjpbwh2ggfjwcipgxqwpbnq00h2dzgcrykp365pddmmxjfkqd"))
	   (patches
		(search-patches
		 "guix-rocm/packages/patches/rocthrust-placement-new.patch"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets)))))
	(inputs (list hipamd rocprim))
	(native-inputs (list rocm-cmake))
    (home-page "https://github.com/ROCm/rocThrust")
    (synopsis "Thrust port for HIP and ROCm")
    (description "Thrust port for HIP and ROCm")
    (license license:asl2.0)))

(define-public composable-kernel
  (let ((version %rocm-version)
		;; Used by the CMake script
		(commit "af471c2308a2eb72a03eaf107b0994a60e443c66")
		(revision "0"))
	(package
      (name "composable-kernel")
      (version version)
      (source (origin
				(method git-fetch)
				(uri (git-reference
					  (url "https://github.com/ROCm/composable_kernel")
					  (commit commit)))
				(file-name (git-file-name name version))
				(sha256
				 (base32
				  "0lw4pyvhxlsa0a1ly6r8968dga51g7l5s6slhvs3nimvjw3nx7zj"))))
      (build-system cmake-build-system)
	  (arguments
	   (list
		#:tests? #f
		#:build-type "Release"
		#:configure-flags
		#~(list
		   (string-append "-DCMAKE_CXX_COMPILER="
						  #$(this-package-input "hipamd")
						  "/bin/hipcc")
		   "-DINSTANCES_ONLY=ON"
		   #$(string-append "-DGPU_TARGETS=" (%amdgpu-targets)))
		#:phases
		#~(modify-phases %standard-phases
			(add-after 'unpack 'patch-cmake
			  (lambda _
				(substitute* "CMakeLists.txt"
				  (("find_package\\(Git.*") "")
				  (("execute_process\\(COMMAND.*GIT_EXECUTABLE.*")
				   (string-append "set(COMMIT_ID " #$commit ")\n")))
				(substitute* "test/CMakeLists.txt"
				  (("include\\(googletest\\)")
				   "find_package(GTest REQUIRED)")))))))
	  (inputs
	   (list hipamd))
	  (native-inputs (list googletest rocm-cmake))
      (home-page "https://github.com/ROCm/composable_kernel")
      (synopsis "")
      (description "")
      (license license:expat))))

(define-public rocmlir
  (package
   (name "rocmlir")
   (version %rocm-version)
   (source (origin
			(method git-fetch)
			(uri (git-reference
				  (url "https://github.com/ROCm/rocMLIR")
				  (commit (string-append "rocm-" version))))
			(file-name (git-file-name name version))
			(sha256
			 (base32
			  "08k93lbyksjc49fn91bk3gr1fwc3nrfpif0yxfj310wh28n0lhkz"))))
   (build-system cmake-build-system)
   (arguments
	(list
	 #:tests? #f
	 #:build-type "Release"
	 #:configure-flags
	 #~(list
		(string-append "-DCMAKE_C_COMPILER="
					   #$(this-package-native-input "clang-rocm")
					   "/bin/clang")
		(string-append "-DCMAKE_CXX_COMPILER="
					   #$(this-package-native-input "clang-rocm")
					   "/bin/clang++")
		"-DBUILD_FAT_LIBROCKCOMPILER=ON"
		#$(string-append "-DROCM_TEST_CHIPSET=" (%amdgpu-targets)))))
   (native-inputs (list clang-rocm hipamd python rocm-cmake))
   (home-page "https://github.com/ROCm/rocMLIR")
   (synopsis "MLIR-based convolution and GEMM kernel generator")
   (description "rocMLIR is a MLIR-based convolution and GEMM kernel
generator targetting AMD hardware.")
   (license license:expat)))

(define-public functionalplus
  (package
    (name "functionalplus")
    (version "0.2.24")
    (source (origin
			  (method git-fetch)
			  (uri (git-reference
					(url "https://github.com/Dobiasd/FunctionalPlus")
					(commit (string-append "v" version))))
			  (file-name (git-file-name name version))
			  (sha256
			   (base32
				"0sx22lcackwfkkphswml0gswfdq6h92q22vhm79fiackb2lay0c9"))))
	(arguments (list #:tests? #f))
    (build-system cmake-build-system)
    (home-page "https://github.com/Dobiasd/FunctionalPlus")
    (synopsis "Functional Programming Library for C++")
    (description "Functional Programming Library for C++")
    (license license:boost1.0)))

;; Version 0.16 breaks MIOpen
(define-public frugally-deep
  (package
    (name "frugally-deep")
    (version "0.15.31")
    (source (origin
			  (method git-fetch)
			  (uri (git-reference
					(url "https://github.com/Dobiasd/frugally-deep")
					(commit (string-append "v" version))))
			  (file-name (git-file-name name version))
			  (sha256
			   (base32
				"0gzkv03kz9p94a9apmds77npvxhmqyahhah4bq5yz3v9pvw6kx10"))))
    (build-system cmake-build-system)
	(arguments (list #:tests? #f))
	(native-inputs (list eigen functionalplus nlohmann-json))
    (home-page "https://github.com/Dobiasd/frugally-deep")
    (synopsis "Header-only library for using Keras (TensorFlow) models
in C++")
    (description "Header-only library for using Keras (TensorFlow)
models in C++")
    (license license:expat)))

(define-public half
  (package
    (name "half")
    (version "2.2.0")
    (source (origin
			  (method url-fetch)
			  (uri (string-append "mirror://sourceforge/half/half/"
								  version "/half-" version ".zip"))
			  (sha256
			   (base32
				"0fsnb82im0b7l0wnkjqiixbqc2z06nys8lq9mdycspxr5x49w78x"))))
    (build-system copy-build-system)
	(arguments
	 (list
	  #:install-plan
	  ''(("include" "include/half"))
	  #:phases
	  #~(modify-phases %standard-phases
		  (add-after 'unpack 'chdir
			(lambda _
			  (chdir ".."))))))
	(native-inputs (list unzip))
    (home-page "https://half.sourceforge.net/")
    (synopsis "Half-precision floating-point library")
    (description "Half-precision floating-point library")
    (license license:expat)))

(define-public roctracer
  (package
   (name "roctracer")
   (version %rocm-version)
   (source (origin
			(method git-fetch)
			(uri (git-reference
				  (url "https://github.com/ROCm/roctracer")
				  (commit (string-append "rocm-" version))))
			(file-name (git-file-name name version))
			(sha256
			 (base32
			  "1sh22vhx7para0ymqgskfl5hslbinxaccillals54pf9dplwvbvb"))))
   (build-system cmake-build-system)
   (arguments
	(list
	 #:tests? #f
	 #:build-type "Release"
	 #:configure-flags
	 #~(list
		(string-append "-DCMAKE_CXX_COMPILER="
					   #$(this-package-native-input "clang-rocm")
					   "/bin/clang++")
		(string-append "-DCMAKE_C_COMPILER="
					   #$(this-package-native-input "clang-rocm")
					   "/bin/clang")
		(string-append "-DCMAKE_MODULE_PATH="
					   #$(this-package-input "hipamd") "/lib/cmake/hip"))
	 #:modules '((srfi srfi-1)
				 (guix build cmake-build-system)
				 (guix build utils))
	 #:phases
	 #~(modify-phases %standard-phases
		 (add-after 'unpack 'fix-experimental
		   (lambda _
			 (substitute* (list
						   "plugin/file/file.cpp"
						   "src/hip_stats/hip_stats.cpp"
						   "src/roctracer/loader.h"
						   "src/tracer_tool/tracer_tool.cpp")
			   (("experimental(/|::)") "")))))))
   (inputs (list hipamd))
   (native-inputs
	(list
	 clang-rocm
	 python
	 python-cppheaderparser
	 rocm-cmake))
   (home-page "https://github.com/ROCm/roctracer")
   (synopsis "ROCm Tracer Callback/Activity Library for Performance
tracing AMD GPUs")
   (description "ROCm Tracer Callback/Activity Library for Performance
tracing AMD GPUs")
   (license license:expat)))

(define-public miopen
  (package
	(name "miopen")
	(version %rocm-version)
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/ROCm/MIOpen")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "04x8dz76hjp4i7mnh3csdr5fy2rj9qh8g3pbkmb2qldsrh216ziw"))
	   (patches
		(search-patches
		 "guix-rocm/packages/patches/miopen-zstd.patch"))))
	(build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 "-DBoost_USE_STATIC_LIBS=OFF"
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets))
		 (string-append "-DHALF_INCLUDE_DIR="
						#$(this-package-native-input "half") "/include"))
	  #:phases
	  #~(modify-phases %standard-phases
		  (add-after 'unpack 'disable-tidy+tests
			(lambda _
			  (substitute* "CMakeLists.txt"
				(("add_subdirectory.*test.*") "")
				(("set\\(MIOPEN_TIDY_ERRORS ALL\\)") ""))))
		  (add-after 'unpack 'disable-clang-tidy
			(lambda _
			  (substitute* "CMakeLists.txt"
				(("add_subdirectory.*test.*") "")))))))
	(inputs
	 (list
	  boost
	  composable-kernel
	  hipamd
	  rocblas
	  rocmlir
	  roctracer
	  sqlite
	  `(,zstd "lib")))
	(native-inputs
	 (list
	  eigen
	  frugally-deep
	  functionalplus
	  googletest
	  half
	  nlohmann-json
	  pkg-config
	  rocm-cmake))
	(home-page "https://github.com/rocm/rocBLAS")
	(synopsis "BLAS implementation for ROCm")
	(description "rocBLAS is the ROCm Basic Linear Algebra Subprograms
(BLAS) library. It is implemented in the HIP programming language.")
	(license license:expat)))

(define-public rocfft
  (package
    (name "rocfft")
	(version %rocm-version)
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/rocm/rocFFT")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "0gsj3lcgppjvppqrldiqgzxsdp0d7vv8rhjg71jww0k76x2lvzf2"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:build-type "Release"
	  #:configure-flags
	  #~(list (string-append "-DCMAKE_CXX_COMPILER="
							 #$(this-package-input "hipamd")
							 "/bin/hipcc")
			  (string-append "-DCMAKE_C_COMPILER="
							 #$(this-package-input "hipamd")
							 "/bin/hipcc")
			  #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets))
			  "-DSQLITE_USE_SYSTEM_PACKAGE=ON")))
	(inputs (list hipamd))
	(native-inputs (list python rocm-cmake sqlite))
    (home-page "https://github.com/rocm/rocFFT")
    (synopsis "FFT implementation for ROCm")
    (description "FFT implementation for ROCm")
    (license license:expat)))

(define-public hipfft
  (package
    (name "hipfft")
    (version %rocm-version)
    (source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/ROCm/hipFFT")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "05iblr9ap7gpqg5n72pdlbddmm4sa78p72sy769ks7nfj1ig82c2"))))
    (build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:build-type "Release"
	  #:configure-flags
	  #~(list
		 (string-append "-DCMAKE_CXX_COMPILER="
						#$(this-package-input "hipamd")
						"/bin/hipcc")
		 #$(string-append "-DAMDGPU_TARGETS=" (%amdgpu-targets)))))
	(inputs (list hipamd rocfft))
	(native-inputs (list rocm-cmake))
    (home-page "https://github.com/ROCm/hipFFT")
    (synopsis "FFT marshalling library with multiple supported backends")
    (description "This package contains a wrapper around rocRAND.")
    (license license:expat)))

(define-public magma-rocm
  (package
    (name "magma-rocm")
    (version "2.8.0")
    (source (origin
			  (method git-fetch)
			  (uri (git-reference
					(url "https://bitbucket.org/icl/magma/")
					(commit (string-append "v" version))))
			  (file-name (git-file-name name version))
			  (sha256
			   (base32
				"0ad657bhlmqynk17hqwwz9qmw41ydgc48s214l88zr1k8vmq35fi"))))
    (build-system cmake-build-system)
	(arguments
	 ;; All four forms are used to specify valid and used targets
	 (let* ((targets_gfx_sc (%amdgpu-targets))
			(targets_gfx_sp (string-join (string-split targets_gfx_sc #\;) " "))
			(targets_sc
			 (string-join
			  (map
			   (lambda (s) (substring s 3))
			   (string-split targets_gfx_sc #\;))
			  " "))
			(targets_sp (string-join (string-split targets_sc #\;) " ")))
	   (list
		#:tests? #f
		#:build-type "Release"
		#:configure-flags
		#~(list
		   "-DMAGMA_ENABLE_HIP=ON"
		   (string-append "-DCMAKE_CXX_COMPILER="
						  #$(this-package-input "hipamd")
						  "/bin/hipcc")
		   (string-append "-DCMAKE_Fortran_COMPILER="
						  #$(this-package-native-input "gfortran")
						  "/bin/gfortran")
		   #$(string-append "-DAMDGPU_TARGETS=" targets_gfx_sc)
		   #$(string-append "-DGPU_TARGET=" targets_gfx_sp)
		   )
		#:phases
		`(modify-phases %standard-phases
		   (add-before 'configure 'generate
			 (lambda _
			   (substitute* "Makefile"
				 (("VALID_GFXS =.*")
				  (string-append "VALID_GFXS = " ,targets_sp "\n")))
			   (substitute* "CMakeLists.txt"
				 (("set.*VALID_GFXS.*")
				  (string-append "set(VALID_GFXS \"" ,targets_sc "\")\n"))
				 (("execute.*get-rocm-version\\.sh.*")
				  "set(ROCM_VERSION \"62000\")\n"))
			   (with-output-to-file "make.inc"
				 (lambda _
				   (display
					(string-append
					 "BACKEND = hip\n"
					 "FORT = true\n"
					 "GPU_TARGET = " ,targets_gfx_sp "\n"))))
			   (invoke "make" "generate"
					   "-j" (number->string (parallel-job-count)))))))))
	(native-inputs (list gfortran perl python-wrapper))
	(inputs
	 (list hipamd
		   hipblas
		   hipsparse
		   openblas))
    (home-page "https://icl.utk.edu/magma/")
    (synopsis "Matrix Algebra on GPU and Multi-core Architectures")
    (description "MAGMA is a collection of linear algebra for
heterogeneous computing. This version is compiled with ROCm.")
    (license license:bsd-3)))
