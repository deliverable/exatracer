;;; Copyright © 2023 Advanced Micro Devices, Inc.
;;; Copyright © 2024 David Elsing <david.elsing@posteo.net>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-rocm packages rocm-hip)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)

  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages version-control)

  #:use-module (guix-rocm packages rocm-base)
  #:use-module (guix-rocm packages rocm-tools))

(define-public rocm-comgr
  (package
	(name "rocm-comgr")
	(version (package-version llvm-rocm))
	(source (package-source llvm-rocm))
	(build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f
	  #:configure-flags ''("-DBUILD_TESTING=OFF")
	  #:phases #~(modify-phases %standard-phases
				   (add-after 'unpack 'chdir
					 (lambda _
					   (chdir "amd/comgr"))))))
	(inputs (list rocm-device-libs))
	(native-inputs (list llvm-rocm lld-rocm clang-rocm))
	(synopsis "The ROCm Code Object Manager")
	(description "The Comgr library provides APIs for compiling and
inspecting AMDGPU code objects.")
	(home-page "https://github.com/RadeonOpenCompute/ROCm-CompilerSupport")
	(license license:ncsa)))

(define-public hip
  (package
	(name "hip")
	(version %rocm-version)
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/Rocm/hip")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "0iw69byvnphlixm79169mqv4kkcbx4a45jwhgf6mw3s563i8vhc4"))
	   (patches
		(search-patches "guix-rocm/packages/patches/hip-headers.patch"))))
	(build-system copy-build-system)
	(arguments
	 (list
	  #:install-plan ''(("." "/"))))
	(synopsis
	 "The Heterogeneous Interface for Portability (HIP) framework")
	(description
	 "The Heterogeneous Interface for Portability (HIP) framework is a
C++ Runtime API and Kernel Language that allows developers to create
portable applications for AMD and NVIDIA GPUs from single source
code.")
	(home-page "https://github.com/ROCm-Developer-Tools/HIP")
	(license license:expat)))

(define-public hipcc
  (package
	(name "hipcc")
	(version (package-version llvm-rocm))
	(source
	 (origin
	   (inherit (package-source llvm-rocm))
	   (patches
		(search-patches "guix-rocm/packages/patches/hipcc-paths.patch"))))
	(build-system cmake-build-system)
	(arguments
	 (list
	  #:build-type "Release"
	  #:tests? #f
	  #:phases
	  '(modify-phases %standard-phases
		 (add-after 'unpack 'chdir
		   (lambda _
			 (chdir "amd/hipcc")))
		 (add-after 'chdir 'adjust-hipcc.pl
		   (lambda _
			 (substitute* "bin/hipcc.pl"
			   (("\\$HIP_ROCCLR_HOME.*=.*")
				(string-append
				 "$ROCMINFO_PATH = $hipvars::ROCMINFO_PATH;\n"
				 "$HIP_ROCCLR_HOME = $hipvars::HIP_ROCCLR_HOME;\n"
				 "$DEVICE_LIB_PATH = $hipvars::DEVICE_LIB_PATH;\n"))
			   (("\\$ROCM_AGENT_ENUM =.*")
				(string-append
				 "$ROCM_AGENT_ENUM = "
				 "\"${ROCMINFO_PATH}/bin/rocm_agent_enumerator\";\n"))
			   (("\\$CMD \\.= .*toolArgs.*" orig)
				(string-append
				 orig "$CMD .= \" --hip-path=${HIP_PATH}\";\n"))))))))
	(propagated-inputs (list rocminfo rocm-toolchain))
	(synopsis "HIP compiler driver (hipcc)")
	(description
	 "The HIP compiler driver (hipcc) is a compiler utility that will
call clang and pass the appropriate include and library options for
the target compiler and HIP infrastructure.")
	(home-page
	 "https://github.com/ROCm-Developer-Tools/HIPCC.git")
	(license license:expat)))

(define clr-source
  (let ((version %rocm-version))
	(origin
	  (method git-fetch)
	  (uri (git-reference
			(url "https://github.com/Rocm/clr")
			(commit (string-append "rocm-" version))))
	  (file-name (git-file-name "clr" version))
	  (sha256
	   (base32
		"16hhacrp45gvmv85nbfh6zamzyjl5hvkb1wjnl01sxnabrc35yl4")))))



(define-public hipamd
  (package
   (name "hipamd")
   (version %rocm-version)
   (source clr-source)
   (build-system cmake-build-system)
   (arguments
    (list
     #:build-type "Release"
     #:tests? #f
     #:configure-flags
     #~(list (string-append
	      "-DHIP_COMMON_DIR=" #$(this-package-native-input "hip"))
	     (string-append
	      "-DHIPCC_BIN_DIR=" #$(this-package-native-input "hipcc") "/bin")
	     "-DCLR_BUILD_HIP=ON"
	     "-DCLR_BUILD_OCL=OFF"
	     "-D__HIP_ENABLE_PCH=OFF"
	     "-DHIP_PLATFORM=amd")
     #:phases
     #~(modify-phases %standard-phases
		      (add-after 'install 'info-version'
			         (lambda _
			           (mkdir (string-append #$output "/.info"))
			           (with-output-to-file
				       (string-append #$output "/.info/version")
				     (lambda _
				       (display (string-append #$version "-0"))))))
		      (add-after 'install 'overwrite-hipvars
			         (lambda _
			           (with-output-to-file
				       (string-append #$output "/bin/hipvars.pm")
				     (lambda _
				       (display
				        (string-append
					 "package hipvars;\n"
					 "$isWindows = 0;\n"
					 "$doubleQuote = \"\\\"\";\n"
					 "$CUDA_PATH = \"\";\n"
					 "$HIP_PLATFORM = \"amd\";\n"
					 "$HIP_COMPILER = \"clang\";\n"
					 "$HIP_RUNTIME = \"rocclr\";\n"
					 "$HIP_CLANG_RUNTIME = \""
					 #$(this-package-input "rocm-toolchain")
					 "\";\n"
					 "$DEVICE_LIB_PATH = \""
					 #$(this-package-input "rocm-toolchain")
					 "/amdgcn/bitcode\";\n"
					 "$HIP_CLANG_PATH = \""
					 #$(this-package-input "rocm-toolchain")
					 "/bin\";\n"
					 "$HIP_PATH = \""
					 #$output
					 "\";\n"
					 "$HIP_VERSION= \""
					 #$version
					 "\";\n"
					 "$ROCMINFO_PATH = \""
					 #$(this-package-input "rocminfo")
					 "\";\n"
					 "$ROCR_RUNTIME_PATH = \""
					 #$(this-package-input "rocm-toolchain")
					 "\";\n"
					 "$HIP_INFO_PATH = \"$HIP_PATH/lib/.hipInfo\";\n"
					 "$HIP_ROCCLR_HOME = $HIP_PATH;\n"
					 "$ROCM_PATH = \""
					 #$(this-package-input "rocm-toolchain")
					 "\";")))))))))
   (native-inputs
    (list mesa
	  libffi
	  git
	  perl
	  python-wrapper
	  python-cppheaderparser
	  hip
	  hipcc))
   (propagated-inputs
    (modify-inputs (package-propagated-inputs hipcc)
	           (append rocm-comgr rocprofiler-register)))
   (synopsis "AMD CLR - Compute Language Runtimes for HIP applications")
   (description
    "AMD Common Language Runtime contains source code for AMD's
compute languages runtimes: HIP and OpenCL. This package is built for HIP only.")
   (home-page "https://github.com/ROCm-Developer-Tools/clr.git")
   (license license:expat)))

(define-public rocm-cmake
  (package
	(name "rocm-cmake")
	(version %rocm-version)
	(source
	 (origin
	   (method git-fetch)
	   (uri (git-reference
			 (url "https://github.com/Rocm/rocm-cmake")
			 (commit (string-append "rocm-" version))))
	   (file-name (git-file-name name version))
	   (sha256
		(base32
		 "05dm7dgg4r5gqbz8sj360nnm348mqxr0fbj3gc0x32l8mw81szf5"))))
	(build-system cmake-build-system)
	(arguments
	 (list
	  #:tests? #f))
	(synopsis "ROCm-CMake is a collection of CMake modules for common
build and development tasks within the ROCm project.")
	(description "ROCm-CMake is a collection of CMake modules for
common build and development tasks within the ROCm project. It is
therefore a build dependency for many of the libraries that comprise
the ROCm platform. ROCm-CMake is not required for building libraries
or programs that use ROCm; it is required for building some of the
libraries that are a part of ROCm.")
	(home-page "https://github.com/RadeonOpenCompute/rocm-cmake.git")
	(license license:expat)))
