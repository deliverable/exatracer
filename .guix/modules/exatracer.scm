(define-module (exatracer)
  #:use-module (gnu packages less)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages engineering)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gdb)
  #:use-module (gnu packages instrumentation)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages version-control)
  #:use-module (guix-rocm packages rocm-base)
  #:use-module (guix-rocm packages rocm-hip)
  #:use-module (guix-rocm packages rocm-tools)
  #:use-module (guix build-system gnu)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix transformations)
  #:use-module (guix utils))


;;; Helpers.
(define vcs-root
  (letrec ((find-git
            (lambda (root)
              (if (false-if-exception (canonicalize-path (string-append root "/.git")))
                  (canonicalize-path root)
                  (let ((parent (dirname root)))
                    (if (string=? parent root)
                        #f
                        (find-git parent)))))))
    (and=>
     (or (current-source-directory)
         (assq-ref (current-source-location) 'filename)
         (raise-exception
          (make-exception
           (make-external-error)
           (make-exception-with-origin 'source-location)
           (make-exception-with-message "could not find filename source location"))))
     (compose find-git canonicalize-path))))

(define vcs-file?
  (or (git-predicate vcs-root)
      (const #t)))

(define with-submodule-source
  (lambda (package)
    (let ((name (package-name package)))
      ((options->transformation
        `((with-source . ,(string-append name "=" vcs-root "/submodules/" name))
          (without-tests . ,name)))
       package))))

(define with-debug-info
  (lambda (package)
    (let ((name (package-name package)))
      ((options->transformation
        `((with-debug-info . ,name)
          (with-configure-flag . ,(string-append name "=CFLAGS=-O0 -ggdb3"))))
       package))))


;;; Latest dev.
(define-public librseq-latest
  (let ((librseq (with-submodule-source librseq)))
    (package/inherit librseq
      (name "librseq-submodule")
      (inputs
       (modify-inputs (package-inputs librseq)
         (append numactl)))
      (native-inputs
       (modify-inputs (package-native-inputs librseq)
         (append pkg-config))))))


;;; Packages.
(define-public libside
  (package
    (name "libside")
    (version "checkout")
    (source (local-file (string-append vcs-root "/submodules/libside") "libside-checkout"
                        #:recursive? #t
                        #:select? (const #t)))
    (build-system gnu-build-system)
    (inputs
     (list librseq-latest))
    (native-inputs
     (list autoconf automake libtool pkg-config))
    (synopsis "")
    (description "")
    (home-page "")
    (license license:expat)))

(define-public libside-debug
  (with-debug-info
   (package/inherit libside
    (name "libside-debug"))))

(define-public exatracer
  (package
    (name "exatracer")
    (version "git")
    (source (local-file vcs-root "exatracer"
                        #:recursive? #t
                        #:select? vcs-file?))
    (build-system gnu-build-system)
    (inputs
     (list libside rocprofiler-sdk))
    (native-inputs
     (list
      babeltrace
      coreutils
      pkg-config
      python
      python-clang-13
      rocr-runtime
      rocm-toolchain
      hipamd))
    (synopsis "")
    (description "")
    (home-page "")
    (license license:expat)))

(package/inherit exatracer
  (name "exatracer-dev")
  (inputs
   (modify-inputs (package-inputs exatracer)
     (replace "libside" libside-debug)))
  (native-inputs
   (modify-inputs (package-native-inputs exatracer)
     (append
      autoconf
      automake
      coreutils
      findutils
      gawk
      gdb
      git
      grep
      less
      libtool
      perf
      strace))))
