<!--
SPDX-FileCopyrightText: 2024 EfficiOS, Inc.

SPDX-License-Identifier: MIT
-->

# Exatracer

The Exatracer is powered by SIDE and can instrument the HIP and HSA runtimes and
ROC-TX.

# Build

See [Build](doc/build.md)

# Usage

See [Usage](doc/usage.md)

# Tutorial

See [Tutorial](doc/tutorial.md)

# Set of ignored functions

Function to ignored for instrumentation can be added to `src/hip-ignores.txt`
and `src/hsa-ignores.txt`.

# Development

If using the `guix` package manager, simply do `./dev-env`.


