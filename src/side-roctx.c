/*
 * Copyright © 2023 AMD
 *
 * All Rights Reserved
 */

#include <limits.h>
#include <stdint.h>

#include "side-roctx.h"

/*
 * This is the table with SIDE tracepoints for roctx.
 */
static roctxCoreApiTable_t side_roctx_core_table;

/*
 * This is an internal copy of the orignal table used by the SIDE wrappers to
 * call the next function for roctx.
 */
static roctxCoreApiTable_t next_roctx_core_table;

static void side_wrapper_roctxMark(const char *message)
{
	side_event(side_event_roctx_mark,
		   side_arg_list(side_arg_gather_string(message)));
	next_roctx_core_table.roctxMarkA_fn(message);
}

static int side_wrapper_roctxRangePush(const char *message)
{
	int level = next_roctx_core_table.roctxRangePushA_fn(message);

	side_event(side_event_roctx_range_push,
		   side_arg_list(side_arg_gather_string(message),
				 side_arg_int(level)));

	return level;
}

static int side_wrapper_roctxRangePop(void)
{
	int level = next_roctx_core_table.roctxRangePop_fn();

	side_event(side_event_roctx_range_pop,
		   side_arg_list(side_arg_int(level)));


	return level;
}

static roctx_range_id_t side_wrapper_roctxRangeStart(const char *message)
{
	roctx_range_id_t id;

	id = next_roctx_core_table.roctxRangeStartA_fn(message);

	side_event(side_event_roctx_range_start,
		   side_arg_list(side_arg_gather_string(message),
				 side_arg_u64(id)));

	return id;
}

static void side_wrapper_roctxRangeStop(roctx_range_id_t id)
{
	side_event(side_event_roctx_range_stop,
		   side_arg_list(side_arg_u64(id)));
	next_roctx_core_table.roctxRangeStop_fn(id);
}

static void side_roctx_install_wrappers(void)
{
	side_roctx_core_table.roctxMarkA_fn = &side_wrapper_roctxMark;
	side_roctx_core_table.roctxRangePushA_fn = &side_wrapper_roctxRangePush;
	side_roctx_core_table.roctxRangePop_fn = &side_wrapper_roctxRangePop;
	side_roctx_core_table.roctxRangeStartA_fn = &side_wrapper_roctxRangeStart;
	side_roctx_core_table.roctxRangeStop_fn = &side_wrapper_roctxRangeStop;
}

