/*
 * Copyright © 2023 AMD
 *
 * All Rights Reserved
 */

#ifndef SIDE_ROCTX_H
#define SIDE_ROCTX_H

#include <side/trace.h>

side_static_event(side_event_roctx_mark, "roctx", "mark", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_gather_string("message", 0, SIDE_TYPE_GATHER_ACCESS_DIRECT),
	)
);

side_static_event(side_event_roctx_range_push, "roctx", "range-push", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_gather_string("message", 0, SIDE_TYPE_GATHER_ACCESS_DIRECT),
		side_field_int("level"),
	)
);

side_static_event(side_event_roctx_range_pop, "roctx", "range-pop", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_int("level"),
	)
);

side_static_event(side_event_roctx_range_start, "roctx", "range-start", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_gather_string("message", 0, SIDE_TYPE_GATHER_ACCESS_DIRECT),
		side_field_u64("id"),
	)
);

side_static_event(side_event_roctx_range_stop, "roctx", "range-stop", SIDE_LOGLEVEL_DEBUG,
	side_field_list(
		side_field_u64("id"),
	)
);

#endif	/* SIDE_ROCTX_H */
